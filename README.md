# DNS Proxy

Simple DNS proxy with DoH, DoT, DoQ and DNSCrypt support

## Github

<https://github.com/AdguardTeam/dnsproxy>

## Download

You can download **dnsproxy** archlinux package from here:

* [dnsproxy-x86_64.pkg.tar.zst](https://gitlab.com/archlinux_build/dnsproxy/-/jobs/artifacts/main/browse?job=run-build)

## Install

VERSION=0.73.2

```bash
sudo pacman -U dnsproxy-${VERSION}-1-x86_64.pkg.tar.zst

```

## Configuration

file: `/etc/conf.d/dnsproxy`

Default upstream via **DNS stamps**:

DNS over TLS - LibreDNS noads

```bash
sdns://AwYAAAAAAAAADjExNi4yMDIuMTc2LjI2IJKMhM-8Xnk8tqEdqW8Ep8zNN16FDHXAYraTUYMR2OcVE2RvdC5saWJyZWRucy5ncjo4NTQ

```

