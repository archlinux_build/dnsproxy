# Maintainer: Evaggelos Balaskas <aur.archlinux.org@disposable.space>

pkgname=dnsproxy-adguard
_pkgname=dnsproxy
pkgver=VERSION
pkgrel=1
pkgdesc="Simple DNS proxy with DoH, DoT, DoQ and DNSCrypt support"
arch=('x86_64')
url="https://github.com/AdguardTeam/${_pkgname}"
license=('Apache')

source=("${_pkgname}.service"
        "${_pkgname}.socket"
        "${_pkgname}.config"
        "https://github.com/AdguardTeam/${_pkgname}/releases/download/v${pkgver}/${_pkgname}-linux-amd64-v${pkgver}.tar.gz")

makedepends=("go" "git")
backup=(etc/conf.d/${_pkgname})

build(){
    cd "${srcdir}/linux-amd64/"
}

package() {
    cd "${srcdir}/linux-amd64/"
    install -D -m755 ${_pkgname} "${pkgdir}/usr/bin/${_pkgname}"
    install -D -m644 "${srcdir}/${_pkgname}.socket"  "${pkgdir}/usr/lib/systemd/system/${_pkgname}.socket"
    install -D -m644 "${srcdir}/${_pkgname}.service" "${pkgdir}/usr/lib/systemd/system/${_pkgname}.service"
    install -D -m644 "${srcdir}/${_pkgname}.config"  "${pkgdir}/etc/conf.d/${_pkgname}"
}

